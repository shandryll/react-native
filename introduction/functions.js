/* Functions */

/* Named Function */
function soma1(num1, num2) {
    return num1 + num2
}
console.log(soma1(5, 2))

/* Anonymous Function */
var soma2 = function (num1, num2) {
    return num1 + num2
}
console.log(soma2(6, 3))

/* ES6 */
const soma3 = (num1, num2) => {
    return num1 + num2
}
console.log(soma3(7, 4))

const soma4 = (num1, num2) => num1 + num2
console.log(soma4(8, 5))